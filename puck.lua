local puck = {
    button = Menu.AddKeyOption({"Hero Specific", "Puck"}, "1. puck Combo button", Enum.ButtonCode.KEY_SPACE),
    orb_pos = {nil, false},
    orb_location = nil,
    escape = false,
    enemy_startcast = 0,
    sleeptick = {0, true}
}

function puck.OnUpdate()
    me = Heroes.GetLocal()
    if me and NPC.GetUnitName(me) ~= "npc_dota_hero_puck" or os.clock() < puck.sleeptick[1] and puck.sleeptick[2] == true then return end
    puck_illusory_orb = NPC.GetAbility(me, "puck_illusory_orb")
    puck_ethereal_jaunt = NPC.GetAbility(me, "puck_ethereal_jaunt")
    puck_waning_rift = NPC.GetAbility(me, "puck_waning_rift")
    puck_phase_shift = NPC.GetAbility(me, "puck_phase_shift")
    puck_dream_coil = NPC.GetAbility(me, "puck_dream_coil")
    blink = NPC.GetItem(me, "item_blink")
    cyclone = NPC.GetItem(me, "item_cyclone")
    --pushes = NPC.GetItem(me, "item_force_staff") or NPC.GetItem(me, "item_hurricane_pike")

    if NPC.GetAbility(me, "special_bonus_cast_range_200") and Ability.GetLevel(NPC.GetAbility(me, "special_bonus_cast_range_200")) > 0 then CastRange = 400 else CastRange = 200 end
    if NPC.GetAbility(me, "special_bonus_unique_puck_6") and Ability.GetLevel(NPC.GetAbility(me, "special_bonus_unique_puck_6")) > 0 then Radius = 825 else Radius = 400 end

    local puck_illusory_orb_castpos = nil
    local puck_waning_rift_castpos = nil
    local puck_dream_coil_castpos = nil
    local enemytower_pos = nil

    for _, orb in pairs(Entity.GetUnitsInRadius(me, 2925, Enum.TeamType.TEAM_FRIEND)) do
        if puck.orb_pos[2] and orb and Entity.IsAlive(orb) and Entity.GetClassName(orb) == "C_DOTA_BaseNPC" then
            puck.orb_pos[1] = NPC.GetAbsOrigin(orb)
        end
    end

    if Entity.GetTeamNum(me) == 2 then
        fountain_pos = Vector(-6700.0, -6700.03125, 512.0)
    else
        fountain_pos = Vector(6900.0, 6649.96875, 512.0)
    end

    for _, tower in pairs(Entity.GetUnitsInRadius(me, 2925, Enum.TeamType.TEAM_ENEMY)) do
        if tower and Entity.IsAlive(tower) and NPC.IsTower(tower) then
            enemytower_pos = NPC.GetAbsOrigin(tower)
        end
    end

    local near_mouse = Input.GetNearestHeroToCursor(Entity.GetTeamNum(me), Enum.TeamType.TEAM_ENEMY), nil, nil, {}
    if not target and near_mouse and NPC.IsPositionInRange(near_mouse, Input.GetWorldCursorPos(), 250) then
        target = near_mouse
    elseif target and (not Entity.IsAlive(me) or not Entity.IsAlive(target) or Entity.IsDormant(target)) then
        target = nil
    end

    local DOD = 0
    for enemyindex = 1, Heroes.Count() do
        local enemy = Heroes.Get(enemyindex)
        if enemy and Entity.IsAlive(enemy) and not Entity.IsDormant(enemy) and not Entity.IsSameTeam(me, enemy) and NPC.GetAbsOrigin(me):Distance(NPC.GetAbsOrigin(enemy)):Length2D() < 1350 then

            for abilityindex = 0, 6 do
                local enemyspell = NPC.GetAbilityByIndex(enemy, abilityindex)
                if enemyspell then
                    if Ability.GetDispellableType(enemyspell) == 1 and Ability.GetLevel(enemyspell) > 0 and Ability.IsReady(enemyspell) then
                        DOD = DOD + 1
                    end
                    if Ability.IsInAbilityPhase(enemyspell) and Ability.GetDispellableType(enemyspell) == 1 and NPC.FindFacingNPC(enemy, nil, Enum.TeamType.TEAM_ENEMY, Ability.GetCastRange(enemyspell) + 150, 15) == me then
                        DOD = DOD + 1
                    end
                    if Ability.GetName(enemyspell) == "nevermore_shadowraze1" and NPC.FindFacingNPC(enemy, nil, Enum.TeamType.TEAM_ENEMY, 450, 25) == me or
                        Ability.GetName(enemyspell) == "nevermore_shadowraze2" and Entity.GetAbsOrigin(me):Distance(Entity.GetAbsOrigin(enemy)):Length2D() > 250 and NPC.FindFacingNPC(enemy, nil, Enum.TeamType.TEAM_ENEMY, 750, 25) == me or
                        Ability.GetName(enemyspell) == "nevermore_shadowraze3" and Entity.GetAbsOrigin(me):Distance(Entity.GetAbsOrigin(enemy)):Length2D() > 450 and NPC.FindFacingNPC(enemy, nil, Enum.TeamType.TEAM_ENEMY, 950, 25) == me or
                        Ability.GetName(enemyspell) == "silencer_curse_of_the_silent" and NPC.FindFacingNPC(enemy, nil, Enum.TeamType.TEAM_ENEMY, 1212, 40) == me or 
                        Ability.GetName(enemyspell) == "void_spirit_astral_step" and NPC.FindFacingNPC(enemy, nil, Enum.TeamType.TEAM_ENEMY, 1100, 15) == me or 
                        Ability.GetName(enemyspell) == "morphling_waveform" and NPC.FindFacingNPC(enemy, nil, Enum.TeamType.TEAM_ENEMY, 1350, 15) == me or 
                        Ability.GetName(enemyspell) == "lina_dragon_slave" and NPC.FindFacingNPC(enemy, nil, Enum.TeamType.TEAM_ENEMY, 1000, 15) == me  then 
                        if puck_phase_shift and Ability.IsReady(puck_phase_shift) and puck.EmemyIsRealAbilityPhase(enemyspell) then
                            Ability.CastNoTarget(puck_phase_shift)
                        end
                    end
                end
            end

            for itemindex = 0, 5 do
                local enemyitem = NPC.GetItemByIndex(enemy, itemindex)
                if enemyitem then
                    if Ability.GetName(enemyitem) == "item_sheepstick" and Ability.IsReady(enemyitem) then
                        DOD = DOD + 1
                    end
                    if Ability.GetName(enemyitem) == "item_black_king_bar" and Ability.IsReady(enemyitem) then
                        DOD = DOD + 1
                    end
                    if Ability.GetName(enemyitem) == "item_orchid" and Ability.IsReady(enemyitem) then
                        DOD = DOD + 1
                    end
                    if Ability.GetName(enemyitem) == "item_bloodthorn" and Ability.IsReady(enemyitem) then
                        DOD = DOD + 1
                    end
                    if Ability.GetName(enemyitem) == "item_abyssal_blade" and Ability.IsReady(enemyitem) then
                        DOD = DOD + 1
                    end
                end
            end

            if NPC.FindFacingNPC(enemy, nil, Enum.TeamType.TEAM_ENEMY, NPC.GetAttackRange(enemy) + 150, 15) == me then
                DOD = DOD + 1
            end

            if Entity.GetHealth(me) / Entity.GetMaxHealth(me) > Entity.GetHealth(enemy) / Entity.GetMaxHealth(enemy) then
                DOD = DOD - 2
            end
            if Entity.GetHealth(enemy) / Entity.GetMaxHealth(enemy) > Entity.GetHealth(me) / Entity.GetMaxHealth(me) then
                --DOD = DOD + 2
            end
            if Entity.GetHealth(me) / Entity.GetMaxHealth(me) < 0.30 then
                DOD = DOD + 5
                puck.escape = true
            end
            local count = Heroes.InRadius(Entity.GetAbsOrigin(enemy), CastRange + 550, Entity.GetTeamNum(me), Enum.TeamType.TEAM_ENEMY)
            local enemies = Heroes.InRadius(Entity.GetAbsOrigin(enemy), 550, Entity.GetTeamNum(me), Enum.TeamType.TEAM_ENEMY)
            if #enemies == #count and #enemies > 1 then
            local pos = {}
            for i, v in ipairs(count) do
                if v then
                    table.insert(pos, {x = Entity.GetAbsOrigin(v):GetX(), y = Entity.GetAbsOrigin(v):GetY()})
                end
            end
            local x, y, c = 0, 0, #pos
            for i = 1, c do
                x = x + pos[i].x
                y = y + pos[i].y
            end
                puck_dream_coil_castpos = Vector(x/c, y/c, 0)
            end

            if target and target == enemy or not target then
                if (DOD < 5 or Entity.GetAbsOrigin(me):Distance(Entity.GetAbsOrigin(enemy)):Length2D() > 900) and not puck.escape then
                    puck_illusory_orb_castpos = Entity.GetAbsOrigin(enemy) + ((Entity.GetAbsOrigin(enemy) - Entity.GetAbsOrigin(me)):Normalized():Scaled(700))
                    puck_waning_rift_castpos = Entity.GetAbsOrigin(enemy) + (Entity.GetAbsOrigin(enemy) - Entity.GetAbsOrigin(me)):Normalized():Scaled(Radius)
                else
                    puck_waning_rift_castpos = Entity.GetAbsOrigin(enemy) + ((fountain_pos - Entity.GetAbsOrigin(enemy)):Normalized():Scaled(Radius))
                    puck_illusory_orb_castpos = Entity.GetAbsOrigin(me) + (fountain_pos - Entity.GetAbsOrigin(me)):Normalized():Scaled(1950)
                end
            end
        
            if puck.escape and NPC.FindFacingNPC(enemy, nil, Enum.TeamType.TEAM_ENEMY, NPC.GetAttackRange(enemy) + 200, 180) == me then
                if puck_illusory_orb_castpos and puck_illusory_orb and Ability.IsReady(puck_illusory_orb) then
                    Ability.CastPosition(puck_illusory_orb, puck_illusory_orb_castpos)
                else
                    if puck_phase_shift and Ability.IsReady(puck_phase_shift) then
                        if not cyclone or (cyclone and Ability.SecondsSinceLastUse(cyclone) < 0 or Ability.SecondsSinceLastUse(cyclone) > 3) then 
                            Ability.CastNoTarget(puck_phase_shift)
                        end
                    else
                        if cyclone and Ability.IsReady(cyclone) then
                            if Ability.SecondsSinceLastUse(puck_illusory_orb) > 0 and Ability.SecondsSinceLastUse(puck_illusory_orb) < 0.25 or blink and Ability.GetCooldown(blink) < 2.5 or puck_phase_shift and Ability.GetCooldown(puck_phase_shift) < 2.5 or puck_illusory_orb and Ability.GetCooldown(puck_illusory_orb) < 2.5 then
                                Ability.CastTarget(cyclone, me)
                            end
                        else
                            if blink and Ability.IsReady(blink) and puck.orb_pos[1] == nil then
                                Ability.CastPosition(blink, Entity.GetAbsOrigin(me) + (fountain_pos - Entity.GetAbsOrigin(me)):Normalized():Scaled(1199))
                            end
                        end
                    end
                end
                puck.orb_location = puck_illusory_orb_castpos
                puck.sleeptick[2] = true
            else
                puck.escape = false
            end
        end
    end

    if puck_ethereal_jaunt and Ability.IsReady(puck_ethereal_jaunt) then
        if puck.orb_location and puck.orb_pos[1] and puck.orb_pos[1]:Distance(puck.orb_location):Length2D() < 225 then
            if not enemytower_pos or enemytower_pos and enemytower_pos:Distance(puck.orb_location):Length2D() > 850 then
                if NPC.HasModifier(me, "modifier_puck_phase_shift") then
                    Player.PrepareUnitOrders(Players.GetLocal(), Enum.UnitOrder.DOTA_UNIT_ORDER_STOP, me, Entity.GetAbsOrigin(me), nil, Enum.PlayerOrderIssuer.DOTA_ORDER_ISSUER_HERO_ONLY)
                end
                Ability.CastNoTarget(puck_ethereal_jaunt)
                puck.orb_pos[1], puck.orb_location, puck.escape, puck.orb_pos[2] = nil, nil, false, false
            end
        end
    end

    if target and puck.TargetChecker(target) then
        if Menu.IsKeyDown(puck.button) then
            puck.AutoDrive(target)
            if not puck.AutoDrive(target) then
                if puck_illusory_orb and Ability.IsReady(puck_illusory_orb) and puck_illusory_orb_castpos then
                    if Entity.GetAbsOrigin(me):Distance(Entity.GetAbsOrigin(target)):Length2D() < 1950 then
                        if #Trees.InRadius(puck_illusory_orb_castpos, 300, true) > 1 then
                            puck_illusory_orb_castpos = Entity.GetAbsOrigin(target) + ((Entity.GetAbsOrigin(target) - Entity.GetAbsOrigin(me)):Normalized():Scaled(225))
                        end
                        Ability.CastPosition(puck_illusory_orb, puck_illusory_orb_castpos)
                        puck.orb_location = puck_illusory_orb_castpos
                    end
                end
                if puck_waning_rift and Ability.IsReady(puck_waning_rift) and puck_waning_rift_castpos then
                    if #Trees.InRadius(puck_waning_rift_castpos, 300) > 1 then
                        puck_waning_rift_castpos = Entity.GetAbsOrigin(target) + Entity.GetRotation(target):GetForward():Normalized():Scaled(150)
                    end
                    if Entity.GetAbsOrigin(me):Distance(Entity.GetAbsOrigin(target)):Length2D() < CastRange + Radius - 50 then
                        Ability.CastPosition(puck_waning_rift, puck_waning_rift_castpos)
                    end
                end
                if puck_phase_shift and Ability.IsReady(puck_phase_shift) then
                end
                if puck_dream_coil and Ability.IsReady(puck_dream_coil) then
                    if puck_dream_coil_castpos and Entity.GetAbsOrigin(me):Distance(puck_dream_coil_castpos):Length2D() < CastRange + 550 then
                        Ability.CastPosition(puck_dream_coil, puck_dream_coil_castpos)
                    end
                end
            end
        else
            target, puck.orb_pos[1], puck.orb_location, puck.escape = nil, nil, nil, false
        end
    end
    puck.sleeptick[1] = os.clock() + 0.078
end

function puck.TargetChecker(target)
    if NPC.HasState(target, Enum.ModifierState.MODIFIER_STATE_MAGIC_IMMUNE) or 
        NPC.HasModifier(target, "modifier_invulnerable") or 
        NPC.HasModifier(target, "modifier_phantomlancer_dopplewalk_phase") or 
        NPC.HasModifier(target, "modifier_eul_cyclone") or 
        NPC.HasModifier(target, "modifier_ursa_enrage") or 
        NPC.HasModifier(target, "modifier_troll_warlord_battle_trance") or 
        NPC.HasModifier(target, "modifier_invoker_tornado") or 
        NPC.HasModifier(target, "modifier_abaddon_borrowed_time") or 
        NPC.GetAbility(target, "phantom_lancer_doppelwalk") and Ability.IsInAbilityPhase(NPC.GetAbility(target, "phantom_lancer_doppelwalk")) 
    then
        return false
    else
        return true
    end
end

function puck.OnProjectile(projectile)
    if not puck_phase_shift or not projectile.target then return end
    for _, proj in pairs(
        {
            "alchemist_unstable_concoction_projectile",
            "bounty_hunter_suriken_toss",
            "chaos_knight_chaos_bolt",
            "dragon_knight_dragon_tail_dragonform_proj",
            "ethereal_blade",
            "medusa_mystic_snake_projectile",
            "medusa_mystic_snake_projectile_initial",
            "morphling_adaptive_strike_agi_proj",
            "nullifier_proj",
            "oracle_fortune_prj",
            "pa_ti8_immortal_stifling_dagger",
            "phantom_assassin_stifling_dagger",
            "phantom_lancer_immortal_ti6_spiritlance",
            "phantomlancer_spiritlance_projectile",
            "skeletonking_hellfireblast",
            "sniper_assassinate",
            "sven_spell_storm_bolt",
            "vengeful_magic_missle",
            "visage_soul_assumption_bolt5",
            "visage_soul_assumption_bolt6",
            "vs_ti8_immortal_magic_missle",
            "windrunner_shackleshot"
        }
    ) do
        if proj and projectile.name == proj and projectile.target == me then
            if puck_phase_shift and Ability.IsReady(puck_phase_shift) then
                Ability.CastNoTarget(puck_phase_shift)
            end
        end
    end
end

function puck.OnLinearProjectileCreate(projectile)
	if not puck.orb_pos[2] and projectile and projectile.source and projectile.name == "puck_illusory_orb" and Entity.IsSameTeam(me, projectile.source) then
        puck.orb_pos[2] = true
    end
end

function puck.EmemyIsRealAbilityPhase(ability)
    if Ability.IsInAbilityPhase(ability) then
        local castpoint = Ability.GetCastPoint(ability)
        if os.clock() - puck.enemy_startcast > castpoint then
            puck.enemy_startcast = os.clock()
            return false
        else
            if os.clock() - puck.enemy_startcast > castpoint / 1.25 then
                return true
            end
        end
        puck.sleeptick[2] = false
    end
    return false
end

function puck.AutoDrive(target)
    if blink and Entity.GetAbsOrigin(me):Distance(Entity.GetAbsOrigin(target)):Length2D() > 900 and puck.orb_pos[1] == nil then
        if Entity.GetAbsOrigin(me):Distance(Entity.GetAbsOrigin(target)):Length2D() < 1199 then
            Ability.CastPosition(blink, Entity.GetAbsOrigin(target) + Entity.GetRotation(target):GetForward():Normalized():Scaled(450))
        end
        if Ability.IsReady(blink) and Ability.IsReady(puck_illusory_orb) and Ability.IsReady(puck_waning_rift) and Ability.IsReady(puck_phase_shift) then
            return true
        else
            return false

        end
    else
        return false
    end
end

return puck